import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    templateUrl: '/dist/views/app-component.html',
    styleUrls: ['./dist/css/app-component.css']
})

export class AppComponent{
    title: string = 'Angular 2 RC1 Startup application'
}